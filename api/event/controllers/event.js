'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const axios = require('axios');

module.exports = {
  import: async ctx => {
    const { data } = await axios.get('https://guides.exhibition-manual.com/events.txt');
    const events = await Promise.all(data.map(eventItem => new Promise(async (resolve, reject) => {
      
	  const { 
		i_eventId, 
		t_courseTitle, 
		t_countryCode,
		t_regionCode,
		t_eventLocation,
		t_courseCode,
		t_categoryCode,
		t_subCategoryCode,
		venue_name,
		course_level,
		month,
		start_date,
		price,
		sap_code,
		brochure_url,
		instructor_name,
		C_field_25 
	 } = eventItem;
	 
      try{
          
        
        const eventData = {
            eventId : i_eventId ,
			title : t_courseTitle,
			countryCode : t_countryCode,
			regionCode : t_regionCode,
			location : t_eventLocation,
			courseCode : t_courseCode,
			categoryCode : t_categoryCode,
			subCategoryCode : t_subCategoryCode,
			venueName :  venue_name,
			courseLevel : course_level,
			month : month,
			startDate : start_date,
			price : price,
			sapCode : sap_code,
			brochureUrl : brochure_url,
			instructorName : instructor_name,
			eventType : C_field_25
        };
		
        // use the strapi services create function to create entry
        const created = await strapi.services.event.create(eventData);
        resolve(created)
      }catch(err){
        reject(err)
      }
    })));
	
	ctx.send(events);
  }
};